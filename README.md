Sutty Postfix Role
==================

Runs a Postfix SMTP server

Role Variables
--------------

`sutty` the main domain name.

`data` the data directory.

`milters` a milter list.

`srs` a postsrsd hostname.

Example Playbook
----------------

```yaml
---
- hosts: "sutty"
  strategy: "free"
  remote_user: "root"
  tasks:
  - name: "Postfix"
    include_role:
      name: "sutty_postfix"
    vars:
      sutty: "sutty.nl"
      milters: "inet:rspamd:11332"
      srs: "postsrsd"
```

Virtual addresses
-----------------

Each host on the `sutty_email` group can have their own virtual
addresses in the format shown on the inventory example.

The special character `@` redirects any address for the given domain to
the actual addresses given on the `addresses` array.

Inventory
---------

A `sutty_email` group is needed to detect to which domains serve email
to, even if they aren't actual Ansible hosts.

```yaml
sutty_email:
  hosts:
    anarres.sutty.nl:
      virtual_addresses:
      - aliases:
        - "@"
        addresses:
        - "sutty@riseup.net"
    athshe.sutty.nl:
      virtual_addresses:
      - aliases:
        - "contacto"
        addresses:
        - "sutty@riseup.net"
        - "contact@sutty.nl"
      - aliases:
        - "@"
        addresses:
        - "sutty@riseup.net"
```

License
-------

MIT-Antifa
